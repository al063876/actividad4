
package actividad4;

public class Hiding {
	public static String cardHide(String card) {
		return card.replaceAll(".(?=.{4})", "*");
	}
}
