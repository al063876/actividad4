
package actividad4;

class Challenge {
  public static String getMiddle(String word) {
    int l = word.length();
		if (l%2==0){
      return word.substring(l/2-1,l/2+1);
    } else {
      return word.substring(l/2,l/2+1);
    }
  }
}