
package actividad4;

public class Vowels {
  public static int getCount(String str) {
    return str.replaceAll("[^aeiouAEIOU]", "").length();
  }
}